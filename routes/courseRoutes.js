const express = require('express');
const router = express.Router();
const auth = require("../auth");

const courseController = require('../controllers/courseController');


// Route for creating a course
router.post("/", auth.verify, (request, response) => {

		const userIsAdmin = auth.decode(request.headers.authorization).isAdmin

		if (userIsAdmin){
			courseController.addCourse(request.body).then(resultFromController => response.send(resultFromController))
		} else {

			response.send('Admin function only!')
		}
})

// Route for retrieving all the courses
router.get("/all", auth.verify, (request, response) => {

	const userData = auth.decode(request.headers.authorization)

	courseController.getAllCourses(userData).then(resultFromController => response.send(resultFromController))
})


// Get all active courses
router.get("/", (request, response) => {

	courseController.getAllActive().then(resultFromController => response.send(resultFromController))
});


// Route for retrieving a specific course
// localhost:4000/courses/############
// courseId = ###########

router.get("/:courseId", (request, response) => {

	console.log(request.params)

	courseController.getCourse(request.params).then(resultFromController => response.send(resultFromController))
});

// Route for updating a course

router.put("/:courseId", auth.verify, (request, response) => {

	courseController.updateCourse(request.params, request.body).then(resultFromController => response.send(resultFromController))
})



// Course Archive
router.put("/:courseId/archive", auth.verify, (request, response) => {

	courseController.courseArchive(request.params, request.body).then(resultFromController => response.send(resultFromController))
})






module.exports = router;