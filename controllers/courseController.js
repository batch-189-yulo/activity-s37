const Course = require('../models/courses');
const auth = require('../auth');

// Create a new course
module.exports.addCourse = (requestBody) => {

	let newCourse = new Course({
		name: requestBody.name,
		description: requestBody.description,
		price: requestBody.price
	})

	return newCourse.save().then((course, error) => {

		if(error) {

			return false

		} else {

			return true
		}
	})
}


//Retrieve All Courses
module.exports.getAllCourses = async (data) => {

	if (data.isAdmin){
			return Course.find({}).then(result => {

		return result
		})
	} else {
		return "Not an Admin"
	}

}

// Get all active courses
module.exports.getAllActive = () => {

	return Course.find({isActive: true}).then(result => {

		return result
	})
}

// 

module.exports.getCourse = (requestParams) => {
	
	return Course.findById(requestParams.courseId).then(result =>{
		return result
	})
}


// 
module.exports.updateCourse = (requestParams, requestBody) => {

	let updatedCourse = {
		name: requestBody.name,
		description: requestBody.description,
		price: requestBody.price
	}

		return Course.findByIdAndUpdate(requestParams.courseId, updatedCourse).then((course, error) => {

			if(error) {
				return false
			} else {
				return true
			}
		})
}


// Course Archive
module.exports.courseArchive = (requestParams, requestBody) => {

	let archiveCourse = {
		isActive: requestBody.isActive
	}

	return Course.findByIdAndUpdate(requestParams.courseId, archiveCourse).then((course, error) => {

		if(error){
			return false
		} else {
			return true
		}
	})
}
