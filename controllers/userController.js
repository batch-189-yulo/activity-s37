const User = require('../models/users');
const Course = require('../models/courses')
const bcrypt = require('bcrypt');
const auth = require('../auth');

// Check if the email already exists
module.exports.checkEmailExists = (requestBody) => {

	return User.find({email: requestBody.email}).then(result => {
		if (result.length > 0) {
			return true
		} else {
			return false
		}
	})
}

// User Registration
module.exports.registerUser = (requestBody) => {

	let newUser = new User ({
		firstName: requestBody.firstName,
		lastName: requestBody.lastName,
		email: requestBody.email,
		mobileNo: requestBody.mobileNo,
		password: bcrypt.hashSync(requestBody.password, 10) //hashSync(<dataToBeHash>, <saltRound>)
	})

	return newUser.save().then((user, error) => {
		if(error) {
			return false
		} else {
			return true
		}
	})
}

//User Authentication
module.exports.loginUser = (requestBody) => {

	return User.findOne({email: requestBody.email}).then(result => {

		if (result == null) {
			return false
		} else {

			const isPasswordCorrect = bcrypt.compareSync(requestBody.password, result.password)
			// compareSync(<data>, <encryptedPassword>) //will return true/false

			if(isPasswordCorrect) {
				return {access: auth.createAccessToken(result)}
			} else {
				return false
			}
		}
	})
}

// Find User Details
module.exports.userDetails = (requestBody) => {
	return User.findOne({_id: requestBody.id})
	.then(result => { 	
		if (result === null) {
			return false
		} else {
			result.password = "";
			return result
		}
	})
	.catch(error => {
		return false
	})
};


// Enroll User to A Class
module.exports.enroll = async (data) => {

	let isUserUpdated = await User.findById(data.userId).then(user => {

		user.enrollments.push({courseId: data.courseId});

		return user.save().then((user, error) => {

			if (error) {
				return false
			} else {
				return true
			}

		})
	})

	let isCourseUpdate = await Course.findById(data.courseId).then(course => {

			course.enrollees.push({userId: data.userId})

			return course.save().then((course,error) => {

				if (error) {
					return false
				} else {
					return true
				}
			})
		})

		if (isUserUpdated && isCourseUpdate) {
			return true
		} else {
			return false
		}

}


